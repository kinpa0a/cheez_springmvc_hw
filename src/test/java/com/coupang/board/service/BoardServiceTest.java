package com.coupang.board.service;

import com.coupang.board.vo.BoardVO;
import com.coupang.database.ICondition;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: coupang
 * Date: 2014. 6. 28.
 * Time: 오후 12:59
 * To change this template use File | Settings | File Templates.
 */
public class BoardServiceTest {
	public static void main(String[] args) {
		BoardService service = new BoardService();

		// 등록
		{
			BoardVO createVO = new BoardVO();
			createVO.setCnt(10);
			createVO.setContent("내용");
			createVO.setRegDate(new Date());
			createVO.setTitle("JSP 참 쉽죠?");
			createVO.setWriter("이동훈");

			boolean createResult = service.addBoard(createVO);
			System.out.println(createResult);
		}

		BoardVO seletOneVO;
		// 단건 조회
		{
			BoardVO conditionVO = new BoardVO();
			conditionVO.setId(1L);
			seletOneVO = service.getBoard(conditionVO);

			System.out.println(seletOneVO);
		}

		// 수정
		{
			seletOneVO.setWriter("진승언");
			boolean updateResult = service.updateBoard(seletOneVO);
			System.out.println("수정 성공 : " + updateResult);
			BoardVO updatedVO = service.getBoard(seletOneVO);
			System.out.println("수정된 정보 : " + updatedVO);
		}

		// 삭제
		{
			boolean deleteResult = service.deleteBoard(seletOneVO);
			System.out.println("삭제 성공 : " + deleteResult);
			BoardVO deletedVO = service.getBoard(seletOneVO);
			System.out.println("삭제된 정보 : " + deletedVO);
		}

		// 등록후 리스트 조회
		{
			for(int k =0; k< 10; k++){
				BoardVO createVO = new BoardVO();
				createVO.setCnt(10);
				createVO.setContent("내용 " + k);
				createVO.setRegDate(new Date());
				createVO.setTitle("제목 " + k);
				createVO.setWriter("이동훈");

				service.addBoard(createVO);
			}//for

			List<BoardVO> boardList = service.getBoardList(new BoardVO());
			for(BoardVO vo : boardList){
				System.out.println( vo );
			}//for
		}

        {
            System.out.println("========== TITLE 검색하기 ==========");

            //inner 클래스, 익명클래스를 쓸 생각을 못했다
            //List 전체가 아니라 VO를 비교할 생각을 못했다
            //모든 메서드는 쓰레드가 실행한다! main method는 main thread가!
            //final 을 써주면 final은 스택이 아니라 다른 영역에 있다.
            final String searchTitle = "제목 3";
            List<BoardVO> boardList
                    = service.getBoardList(new ICondition<BoardVO>(){

                @Override
                public boolean filter(BoardVO boardVO) {
                    return searchTitle.equals(boardVO.getTitle());  //To change body of implemented methods use File | Settings | File Templates.
                }
            });

            for(BoardVO vo : boardList){
                System.out.println( vo );
            }//for
        }

//        File dir = new File(".");
//        File[] subFiles = dir.listFiles(new FilenameFilter() {
//            @Override
//            public boolean accept(File dir, String name) {
//                return name.endsWith("txt");
//            }
//        });
	}
}
