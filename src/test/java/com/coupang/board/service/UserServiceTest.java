package com.coupang.board.service;

import com.coupang.user.service.UserService;
import com.coupang.user.vo.UserVO;

import java.util.List;

public class UserServiceTest {
	public static void main(String[] args) {
		UserService service = new UserService();

		// 등록
		{
			UserVO createVO = new UserVO();
			createVO.setName("이동훈");
			createVO.setPassword("1111");
			createVO.setRole("User");
			createVO.setUserId("a1111");

			boolean createResult = service.addUser(createVO);
			System.out.println(createResult);
		}

		UserVO seletOneVO;
		// 단건 조회
		{
			UserVO conditionVO = new UserVO();
			conditionVO.setId(1L);
			seletOneVO = service.getUser(conditionVO);

			System.out.println(seletOneVO);
		}

		// 수정
		{
			seletOneVO.setName("진승언");
			boolean updateResult = service.updateUser(seletOneVO);
			System.out.println("수정 성공 : " + updateResult);
			UserVO updatedVO = service.getUser(seletOneVO);
			System.out.println("수정된 정보 : " + updatedVO);
		}

		// 삭제
		{
			boolean deleteResult = service.deleteUser(seletOneVO);
			System.out.println("삭제 성공 : " + deleteResult);
			UserVO deletedVO = service.getUser(seletOneVO);
			System.out.println("삭제된 정보 : " + deletedVO);
		}

		// 등록후 리스트 조회
		{
			for(int k =0; k< 10; k++){
				UserVO createVO = new UserVO();
				createVO.setName("이동훈 " + k);
				createVO.setPassword("1111");
				createVO.setRole("User");
				createVO.setUserId("a" + k);

				service.addUser(createVO);
			}//for

			List<UserVO> userList = service.getUserList(new UserVO());
			for(UserVO vo : userList){
				System.out.println( vo );
			}//for
		}

	}
}
