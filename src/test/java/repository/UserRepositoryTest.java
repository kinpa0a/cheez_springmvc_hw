package repository;

import com.coupang.cheez.repository.UserRepository;
import com.coupang.cheez.vo.UserVO;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 1.
 * Time: 오후 8:40
 * To change this template use File | Settings | File Templates.
 */
public class UserRepositoryTest {

    //FIXME 셋업할 때 insert를 사용하는게 테스트의 독립성을 해침.

    public static Map<Long, UserVO> userMap;
    public static UserRepository userRepository;

    int beforeMapSize;
    int afterMapSize;

    @BeforeClass
    public static void setUpOnce() throws Exception {
        UserVO vo1 = new UserVO("hi");
        UserVO vo2 = new UserVO("bye");
        UserVO vo3 = new UserVO("good");

        userRepository = UserRepository.getInstance();

        userMap = userRepository.getTMap();

        userRepository.insert(vo1);
        userRepository.insert(vo2);
        userRepository.insert(vo3);
    }


    @Before
    public void setUp() throws Exception {
        beforeMapSize = userMap.size();
    }

    @Test
    public void testFindAll() throws Exception {
        List<UserVO> userVOList = userRepository.findAll();

        afterMapSize = userVOList.size();
        assertThat(afterMapSize, is(beforeMapSize));
    }

    @Test
    public void testDelete() throws Exception {
        UserVO userVO = new UserVO();
        userVO.setId(2l);
        userRepository.delete(userVO);

        afterMapSize = userMap.size();

        assertThat(afterMapSize, is(beforeMapSize - 1));
    }

    @Test
    public void testFindOne() throws Exception {
        UserVO userVO = userRepository.findOne(1l);

        assertThat(userVO.getName(), is("hi"));
        assertThat(userVO.getId(), is(1l));
    }

    @Test
    public void testInsert() throws Exception {
        UserVO vo4 = new UserVO("good");
        UserVO vo5 = new UserVO("omg");

        userRepository.insert(vo4);
        userRepository.insert(vo5);

        afterMapSize = userMap.size();

        assertThat(afterMapSize, is(beforeMapSize + 1));
    }

    @Test
    public void testUpdate_newValue() throws Exception {
        UserVO userVO = new UserVO();
        userVO.setId(1l);
        userVO.setName("good");
        userRepository.update(userVO);

        assertThat(userMap.get(1l).getName(), is("hi"));
    }

    @Test
    public void testUpdate_duplicatedValue() throws Exception {
        UserVO userVO = new UserVO();
        userVO.setId(1l);
        userVO.setName("rururu");
        userRepository.update(userVO);

        assertThat(userVO.getName(), is("rururu"));
    }
}
