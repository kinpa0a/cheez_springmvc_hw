package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


//스프링은 빈컨테이너
//선: 웹서버(아파치, 엔진엑스) - 후: WAS 의 구조가 많음
//WAS는 컨테이너를 관리함(JSP, Servlet, HTML ... conatainer 등을 관리하는 웹 어플리케이션 서버)
//HTML은 웹서버(정적 파일들만 따로 관리해주는 서버)에서 관리하는 경우가 많음


@Controller
@RequestMapping("/")
public class HelloController {
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "hello";
	}
}