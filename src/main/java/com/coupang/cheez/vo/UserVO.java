package com.coupang.cheez.vo;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 2.
 * Time: 오후 6:25
 * To change this template use File | Settings | File Templates.
 */
public class UserVO extends VO {

    String name;

    public UserVO() {
    }

    public UserVO(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserVO)) return false;

        UserVO userVO = (UserVO) o;

        if (!name.equals(userVO.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
