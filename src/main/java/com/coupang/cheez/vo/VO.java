package com.coupang.cheez.vo;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 1.
 * Time: 오후 3:14
 * To change this template use File | Settings | File Templates.
 */
public abstract class VO {

    private Long id;

    VO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
