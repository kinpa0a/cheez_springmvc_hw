package com.coupang.cheez.repository;

import com.coupang.cheez.vo.UserVO;

import java.util.HashMap;

public class UserRepository extends Repository<UserVO> {

    private static UserRepository userRepository;

    private UserRepository() {
    }

    public static synchronized UserRepository getInstance() {
        if (userRepository == null) {
            userRepository = new UserRepository();
        }
        userRepository.setTMap(new HashMap<Long, UserVO>());
        return userRepository;
    }

}
