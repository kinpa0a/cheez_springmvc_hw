package com.coupang.cheez.repository;

import com.coupang.cheez.util.Counter;
import com.coupang.cheez.vo.VO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Repository<T extends VO> {

    private Map<Long, T> tMap = new HashMap<Long, T>();
    private Counter counter = new Counter();

    protected void setTMap(Map<Long, T> tMap) {
        this.tMap = tMap;
    }

    public Map<Long, T> getTMap() {
        return tMap;
    }

    public List<T> findAll() {
        return new ArrayList(tMap.values());
    }

    public boolean delete(T t) {
        if (tMap.containsKey(t.getId())) {
            tMap.remove(t.getId());
            return true;
        }
        return false;
    }

    public T findOne(Long id) throws IllegalAccessException, InstantiationException {
        if (id == null) return null;
        if (tMap.containsKey(id)) {
            return tMap.get(id);
        }
        return null;
    }

    public synchronized boolean insert(T t) {
        if (tMap.containsValue(t)) {
            return false;
        }
        t.setId(counter.getCounter());
        tMap.put(t.getId(), t);
        return true;
    }

    public boolean update(T t) {
        if (t == null) return false;
        if (!tMap.containsKey(t.getId()) || tMap.containsValue(t)) {
            return false;
        }
        tMap.put(t.getId(), t);
        return true;
    }
}
