package com.coupang.database;

import com.coupang.user.vo.UserVO;

import java.util.List;

public interface IDatabase<V extends IValueObject> {

	public abstract List<V> findAll();

	public abstract List<V> findAll(ICondition iCondition);

	public abstract V findOne(Long id);

	public abstract boolean insert(V value);

	public abstract boolean update(V value);

	public abstract boolean delete(V value);

    V findOneFromCondition(ICondition<V> iCondition);
}
