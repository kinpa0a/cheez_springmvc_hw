package com.coupang.database;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오후 12:24
 * To change this template use File | Settings | File Templates.
 */
public interface ICondition<T extends IValueObject> {

    public boolean filter(T t);

}
