package com.coupang.database;

import java.util.*;

/**
 * 메모리 데이터 베이스를 만드세요.
 * 
 * 거창하지 않습니다. 테이블 하나를 만들고 해당 테이블에 데이터를 저장할 수 있도록 클래스를 정의 하세요.
 * 하나의 클래스가 하나의 테이블과 대응됩니다. 
 * 해당 클래스는 아래 메서드를 구현해야 합니다.
 *   - public List<VO> findAll()
 *   - public VO findOne(Long id);
 *   - public boolean insert(VO vo);
 *   - public boolean update(VO vo);
 *   - public boolean delete(VO vo);
 * 데이터가 영속성을 갖을 필요는 없습니다. 그냥 메모리에서 내려가면 날아가는 형태입니다.
 * MySQL과 비슷하게 PK는 자동증가 처리 합니다. (Long or long 값 사용)
 * 
 * @author coupang
 *
 * @param <V>
 */
public class MemoryDatabase<V extends IValueObject> implements IDatabase<V> {

    //넣은 순서대로 나오기 위해서 LinkedHashMap()을 사용함
	private LongKeyGenerator longKeyGenerator = new LongKeyGenerator();
	private Map<Long, V> database = new LinkedHashMap();

	/* (non-Javadoc)
	 * @see com.coupang.dao.IDatabase#findAll()
	 */
	@Override
	public List<V> findAll() {
		//return new ArrayList<V>(database.values());
		List<V> resultList = new ArrayList<V>(database.values());
		Collections.reverse(resultList);
		return resultList;
	}

	public List<V> findAll(ICondition icondition) {
		//return new ArrayList<V>(database.values());
		List<V> allList = new ArrayList<V>(database.values());
		List<V> resultList = new ArrayList<V>();
        for(V v: allList) {
            if(icondition.filter(v)) {
                resultList.add(v);
            }
        }
		return resultList;
	}

    /* (non-Javadoc)
     * @see com.coupang.dao.IDatabase#findOne(java.lang.Long)
     */
	@Override
	public V findOne(Long id) {
		return database.get(id);
	}

	/* (non-Javadoc)
	 * @see com.coupang.dao.IDatabase#create(V)
	 */
	@Override
	public boolean insert(V value) {
		Long id = longKeyGenerator.getNextId();
		value.setId(id);
		V originalValue = database.put(id, value);
		if ( originalValue == null ){
			return true; 
		}
		return false; 
	}

	/* (non-Javadoc)
	 * @see com.coupang.dao.IDatabase#update(V)
	 */
	@Override
	public boolean update(V value) {
		Long id = value.getId();
		if (!database.containsKey(id)) {
			return false;
		}
		database.put(id, value);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.coupang.dao.IDatabase#delete(V)
	 */
	@Override
	public boolean delete(V value) {
		Long id = value.getId();
		if (!database.containsKey(id)) {
			return false;
		}
		database.remove(id);
		return true;
	}

    @Override
    public V findOneFromCondition(ICondition<V> condition) {
        List<V> resultList = findAll(condition);
        if(resultList.isEmpty()){
            return null;
        }
        return resultList.get(0);
    }


}
