package com.coupang.database;

public class LongKeyGenerator {

	public LongKeyGenerator() {
		super();
	}
	
	public LongKeyGenerator(long lastId) {
		super();
		this.lastId = lastId;
	}

	private long lastId;

	public synchronized long getNextId() {
		return ++lastId;
	}

	public synchronized long getLastId() {
		return lastId;
	}

	public synchronized void setLastId(long longValue) {
		lastId = longValue;
	}

}
