package com.coupang.database;

import java.util.HashMap;
import java.util.Map;

public class MemoryDatabaseFactory {
	private static Map<Class<? extends IValueObject>, IDatabase<? extends IValueObject>> databaseMap = new HashMap<Class<? extends IValueObject>, IDatabase<? extends IValueObject>>();

	@SuppressWarnings("unchecked")
	public static <V extends IValueObject> IDatabase<V>
            getDatabase(Class<V> annotationClass) {

		if (!databaseMap.containsKey(annotationClass)) {
			databaseMap.put(annotationClass, new MemoryDatabase<V>());
		}

		return (IDatabase<V>) databaseMap.get(annotationClass);
	}
}
