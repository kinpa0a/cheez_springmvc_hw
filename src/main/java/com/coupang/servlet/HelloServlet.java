package com.coupang.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오전 10:09
 * To change this template use File | Settings | File Templates.
 */
//@WebServlet(name = "HelloServlet")
public class HelloServlet extends HttpServlet {

    //멤버 변수로 선언?
    //멀티쓰레드 환경에서 서블릿이 하나만 생성되고 공유 되기 때문에 상태를 멤버 변수로 선언하면 안된다.

    //servlet life cycle
    //init 객체가 생성되고 한 번만 초기화된다
    //request하면 service() 메서드가 호출되고 get/post를 선택하도록 처리가 되어있다.
    //destroy 객체가 소멸될 때 한 번만 호출된다.

    public HelloServlet() {
        System.out.println("HelloServlet Construct");
    }

    @Override
    public void init() throws ServletException {
        System.out.println("init()");
        super.init();
        //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void destroy() {
        System.out.println("destroy()");
        super.destroy();    //To change body of overridden methods use File | Settings | File Templates.
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("doGet or service");
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        for(int k=0; k< 10; k++) {
            out.println("Hello Servlet " + k + "<br />");
        }
    }

}
