package com.coupang.user.dao;

import com.coupang.database.ICondition;
import com.coupang.database.IDatabase;
import com.coupang.database.MemoryDatabaseFactory;
import com.coupang.user.vo.UserVO;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오전 11:51
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class UserDao {
    IDatabase<UserVO> database = MemoryDatabaseFactory.getDatabase(UserVO.class);

    public boolean addUser(UserVO vo) {
        return database.insert(vo);
    }

    public boolean updateUser(UserVO vo) {
        return database.update(vo);
    }

    public boolean deleteUser(UserVO vo) {
        return database.delete(vo);
    }

    public UserVO getUser(UserVO vo) {
        return database.findOne(vo.getId());
    }

    public List<UserVO> getUserList(UserVO vo) {
        return database.findAll();
    }

    public List<UserVO> getUserList(ICondition iCondition) {
        return database.findAll(iCondition);
    }

    public UserVO getUser(ICondition<UserVO> iCondition) {
        return database.findOneFromCondition(iCondition);
    }
}
