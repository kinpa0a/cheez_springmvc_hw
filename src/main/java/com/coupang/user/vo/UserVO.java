package com.coupang.user.vo;

import com.coupang.database.IValueObject;

public class UserVO implements IValueObject {
	private Long id;
	private String userId;
	private String password;
	private String name;
	private String role;

    // 검색용 필드
    private String searchCondition;
    private String searchKeyword;

    public String getSearchCondition() {
        return searchCondition;
    }

    public void setSearchCondition(String searchCondition) {
        this.searchCondition = searchCondition;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    @Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "UserVO [id=" + id
				+ ", userId=" + userId
				+ ", password=" + password
				+ ", name=" + name
				+ ", role=" + role + "]";
	}

}
