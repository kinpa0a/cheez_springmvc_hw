//package com.coupang.user.controller;
//
//import com.coupang.user.service.UserService;
//import com.coupang.user.vo.UserVO;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.List;
//
///**
// * Created by Coupang on 2014. 7. 7..
// */
//@Controller
//public class UserControllerByModel2 extends HttpServlet {
//
//    @Autowired
//    UserService userService;
//
//    @Override
//    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//
//        // /user/login => 로그인 폼을 보여준다.
//        // /user/loginProc => 로그인 액션을 한다.
//        // /user/logout => 로그아웃웃
//
//        String uri = req.getRequestURI();
//
//        if("/user/add".equals(uri)) {
//            req.getRequestDispatcher("/addUser.jsp").forward(req, res);
//        }
//
//        if("/user/add_proc".equals(uri)) {
//            add_proc(req, res);
//        }
//
//        if("/user/getUserList".equals(uri)) {
//            getUserList(req, res);
//        }
//
//        if("/user/getUser".equals(uri)) {
//            getUser(req, res);
//        }
//
//        if("/user/login".equals(uri)) {
//            login(req, res);
//        }
//
//        if("/user/logout".equals(uri)) {
//            logout(req, res);
//        }
//
//        if("/user/login_proc".equals(uri)) {
//            login_proc(req, res);
//        }
//    }
//
//    private void getUser(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        String id = req.getParameter("seq");
//
//        UserService userService = new UserService();
//        UserVO userVO = new UserVO();
//        userVO.setId(Long.parseLong(id));
//        userVO = userService.getUser(userVO);
//
//        req.setAttribute("userVO", userVO);
//        req.getRequestDispatcher("/getUser.jsp").forward(req, res);
//    }
//
//    private void getUserList(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        String searchCondition = req.getParameter("searchCondition");
//        String searchKeyword = req.getParameter("searchKeyword");
//
//        UserVO vo = new UserVO();
//        vo.setSearchCondition(searchCondition);
//        vo.setSearchKeyword(searchKeyword);
//
//        UserService userService = new UserService();
//        List<UserVO> userList = userService.getUserList(vo);
//
//        req.setAttribute("userList", userList);
//        req.getRequestDispatcher("/getUserList.jsp").forward(req, res);
//    }
//
//    private void add_proc(HttpServletRequest req, HttpServletResponse res) throws IOException {
//        String id = req.getParameter("id");
//        String password = req.getParameter("password");
//        String name = req.getParameter("name");
//        String role = req.getParameter("role");
//
//        UserVO userVO = new UserVO();
//        userVO.setUserId(id);
//        userVO.setPassword(password);
//        userVO.setName(name);
//        userVO.setRole(role);
//
//        UserService userService = new UserService();
//        userService.addUser(userVO);
//
//        res.sendRedirect("/user/getUserList");
//    }
//
//    private void login_proc(HttpServletRequest req, HttpServletResponse res) throws IOException {
//        String userId = req.getParameter("userId");
//        String password = req.getParameter("password");
//        String isSave = req.getParameter("isSave");
//
//        UserVO vo = new UserVO();
//        vo.setUserId(userId);
//        vo.setPassword(password);
//
//        UserVO userVO = userService.getUser(vo);
//
//        if(userVO == null) {
//            res.sendRedirect("/user/login");
//            return;
//        }
//
//        req.getSession().setAttribute("USER", userVO);
//
//        if("Y".equals(isSave)) {
//            Cookie isSaveCookie = new Cookie("userId", userId);
//            isSaveCookie.setMaxAge(60 * 60);
//            isSaveCookie.setPath("/");
//            res.addCookie(isSaveCookie);
//        }
//
//        res.sendRedirect("/board/getBoardList");
//        return;
//    }
//
//    private void logout(HttpServletRequest req, HttpServletResponse res) throws IOException {
//        req.getSession().invalidate();
//
//        //sendredirect는 브라우저에서 보내는 호출, jsp를 직접적으로 호출하지 않는다.
//        res.sendRedirect("/user/login");
//    }
//
//    private void login(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//
//        String savedId = "";
//        String isSave = "";
//
//        Cookie[] cookies = req.getCookies();
//        for (Cookie cookie : cookies) {
//            if("userId".equals(cookie.getName())) {
//                savedId = cookie.getValue();
//                isSave = "checked";
//                break;
//            }
//        }
//
//        //Object Scope
//        //서블릿과 jsp가 공유하는 객체
//        //리퀘스트는 요청 한개당 하나씩 생성됨, 세션은 하나를 모두 공유
//        //응답이 나가면 리퀘스트는 사라짐! 완벽한 임시공간
//        //리퀘스트를 쓰시오!!!!!!!
//        //request > page > session > application
//
//        req.setAttribute("savedId", savedId);
//        req.setAttribute("isSave", isSave);
//
//        req.getRequestDispatcher("/login.jsp").forward(req, res);
//    }
//}
