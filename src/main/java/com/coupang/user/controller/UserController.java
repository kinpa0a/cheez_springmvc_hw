package com.coupang.user.controller;

import com.coupang.user.service.UserService;
import com.coupang.user.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

/**
 * Created by Coupang on 2014. 7. 7..
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;

    //빈 생성 -> DI -> 마지막에 호출된다.
    @PostConstruct
    private void init() {
//		UserService userService = new UserService();

        {
            UserVO userVO = new UserVO();
            userVO.setUserId("admin");
            userVO.setPassword("1111");
            userVO.setName("관리자");
            userVO.setRole("Admin");

            userService.addUser(userVO);
        }

        {
            UserVO userVO = new UserVO();
            userVO.setUserId("test");
            userVO.setPassword("1111");
            userVO.setName("테스트");
            userVO.setRole("User");

            userService.addUser(userVO);
        }
    }

    @RequestMapping("/user/updateUser_proc")
    private String updateUser_proc(UserVO paramVO) throws IOException {
        UserVO userVO = userService.getUser(paramVO);
        userVO.setUserId(paramVO.getUserId());
        userVO.setRole(paramVO.getRole());
        userService.updateUser(userVO);

        return "redirect:/user/getUserList";
    }

    @RequestMapping("/user/getUser")
    private String getUser(@RequestParam(defaultValue = "") String id, UserVO paramVO, Model model) throws ServletException, IOException {

        if(id.isEmpty()) {
            return "user/getUserList";
        }

        paramVO.setId(Long.parseLong(id));
        paramVO = userService.getUser(paramVO);

        model.addAttribute("userVO", paramVO);

        return "user/getUser";
    }

    @RequestMapping("/user/getUserList")
    private String getUserList(UserVO paramVO, Model model) throws ServletException, IOException {

        List<UserVO> userList = userService.getUserList(paramVO);

        model.addAttribute("userList", userList);

        return "user/getUserList";
    }

    @RequestMapping("/user/addUser")
    private String addUser() throws ServletException, IOException {
        return "user/addUser";
    }

    @RequestMapping("/user/add_proc")
    private String add_proc(UserVO userVO) throws IOException {
        userService.addUser(userVO);

        return "redirect:/user/getUserList";
    }

    @RequestMapping("/user/deleteUser_proc")
    private String delete_proc(@RequestParam(defaultValue = "") String id) throws IOException {

        if(!id.isEmpty()) {
            UserVO userVO = new UserVO();
            userVO.setId(Long.parseLong(id));

            userService.deleteUser(userVO);
        }

        return "redirect:/user/getUserList";
    }

    @RequestMapping("/user/login_proc")
    private String login_proc(HttpSession session, UserVO paramVO, @RequestParam(value = "isSave", defaultValue = "N") String isSave,
                              HttpServletResponse res) throws IOException {

        UserVO userVO = userService.getUser(paramVO);

        if(userVO == null) {
            return "redirect:/user/login";
        }

        session.setAttribute("USER", userVO);

        if("Y".equals(isSave)) {
            Cookie isSaveCookie = new Cookie("userId", paramVO.getUserId());
            isSaveCookie.setMaxAge(60 * 60);
            isSaveCookie.setPath("/");
            res.addCookie(isSaveCookie);
        }

        return "redirect:/board/getBoardList";
    }

    @RequestMapping("/user/logout")
    private String logout(HttpSession session) throws IOException {
        session.invalidate();

        //sendredirect는 브라우저에서 보내는 호출, jsp를 직접적으로 호출하지 않는다.
        return "redirect:/user/login";
    }

    @RequestMapping("/user/login")
    private ModelAndView login(HttpServletRequest req) throws ServletException, IOException {

        String savedId = "";
        String isSave = "";

        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            if("userId".equals(cookie.getName())) {
                savedId = cookie.getValue();
                isSave = "checked";
                break;
            }
        }

        ModelAndView mav = new ModelAndView("user/login");
        mav.addObject("savedId", savedId);
        mav.addObject("isSave", isSave);

        return mav;
    }

}
