package com.coupang.user.service;

import com.coupang.user.dao.UserDao;
import com.coupang.user.vo.UserVO;
import com.coupang.database.ICondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오전 11:44
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService {

    //초기화 블럭
    //변수 선언 다음에 호출이 된다
    //static 초기화 블럭은 딱 한 번만 호출된다.
//    {
//        DataInitializer.getInstance();
//    }

    @Autowired
    UserDao UserDAO;

    public boolean addUser(UserVO vo) {
        return UserDAO.addUser(vo);
    }

    public boolean updateUser(UserVO vo) {
        return UserDAO.updateUser(vo);
    }

    public boolean deleteUser(UserVO vo) {
        return UserDAO.deleteUser(vo);
    }

    public UserVO getUser(final UserVO vo) {

        if(vo.getId() != null) {
            return UserDAO.getUser(vo);
        }

        return UserDAO.getUser(new ICondition<UserVO>() {
            @Override
            public boolean filter(UserVO userVO) {
                return userVO.getUserId().equals(vo.getUserId())
                        && userVO.getPassword().equals(vo.getPassword());
            }
        });
    }

    public List<UserVO> getUserList(UserVO vo) {

        final String searchCondition = vo.getSearchCondition();
        final String searchKeyword = vo.getSearchKeyword();

        if(vo.getSearchCondition() == null || vo.getSearchKeyword() == null) {
            return UserDAO.getUserList(vo);
        }

        ICondition<UserVO> iCondition = null;

        if(vo.getSearchCondition() != null) {
            if("USERID".equals(searchCondition)) {
                iCondition= new ICondition<UserVO>() {
                    @Override
                    public boolean filter(UserVO vo) {
                        return vo.getUserId().contains(searchKeyword);
                    }
                };
            } else if("NAME".equals(searchCondition)) {
                iCondition = new ICondition<UserVO>() {
                    @Override
                    public boolean filter(UserVO vo) {
                        return vo.getName().contains(searchKeyword);
                    }
                };
            } else {
                iCondition = new ICondition<UserVO>() {
                    @Override
                    public boolean filter(UserVO vo) {
                        return vo.getRole().contains(searchKeyword);
                    }
                };
            }
        }
        return UserDAO.getUserList(iCondition);
    }

    public List<UserVO> getUserList(ICondition<UserVO> iCondition) {
        return UserDAO.getUserList(iCondition);
    }
}
