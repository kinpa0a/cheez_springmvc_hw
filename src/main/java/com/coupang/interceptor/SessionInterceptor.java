//package com.coupang.interceptor;
//
//import com.coupang.user.vo.UserVO;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.Set;
//
///**
// * Created by Coupang on 2014. 7. 7..
// */
//public class SessionInterceptor extends HandlerInterceptorAdapter {
//
//    //필터는 스프링 컨테이너 가기 전! 디비 접속도 안됨!
//
//    //로그인해야하는 uri를 관리하기 위한 셋을 초기화한다.
//    private Set<String> uriMapping = null;
//
//    public void setUriMapping(Set<String> uriMapping) {
//        this.uriMapping = uriMapping;
//    }
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        String uri = request.getRequestURI();
//
//        //로그인 관리 대상 페이지가 아니라면 그냥 넘어가라라
//        if(!uriMapping.contains(uri)) {
//            return true;
//        }
//
//        UserVO userVO = (UserVO) request.getSession().getAttribute("USER");
//        if(userVO == null) {
//            response.sendRedirect("/user/login");
//            return false;
//        }
//        return true;
//    }
//}
