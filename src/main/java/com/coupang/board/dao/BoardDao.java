package com.coupang.board.dao;

import com.coupang.board.vo.BoardVO;
import com.coupang.database.ICondition;
import com.coupang.database.IDatabase;
import com.coupang.database.MemoryDatabaseFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오전 11:51
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class BoardDao {
    IDatabase<BoardVO> database = MemoryDatabaseFactory.getDatabase(BoardVO.class);

    public boolean addBoard(BoardVO vo) {
        return database.insert(vo);
    }

    public boolean updateBoard(BoardVO vo) {
        return database.update(vo);
    }

    public boolean deleteBoard(BoardVO vo) {
        return database.delete(vo);
    }

    public BoardVO getBoard(BoardVO vo) {
        return database.findOne(vo.getId());
    }

    public List<BoardVO> getBoardList(BoardVO vo) {
        return database.findAll();
    }

    public List<BoardVO> getBoardList(ICondition iCondition) {
        return database.findAll(iCondition);
    }
}
