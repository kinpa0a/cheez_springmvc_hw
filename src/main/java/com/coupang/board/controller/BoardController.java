package com.coupang.board.controller;

import com.coupang.board.service.BoardService;
import com.coupang.board.vo.BoardVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by Coupang on 2014. 7. 7..
 */
@Controller
@RequestMapping("/board")
public class BoardController extends HttpServlet{

    @Autowired
    BoardService boardService;

    @RequestMapping("/updateBoard_proc")
    private String updateBoard_proc(BoardVO paramVO) throws IOException {
        BoardVO boardVO = boardService.getBoard(paramVO);
        boardVO.setTitle(paramVO.getTitle());
        boardVO.setContent(paramVO.getContent());
        boardService.updateBoard(boardVO);
        return "redirect:/board/getBoardList";
    }

    @RequestMapping("/getBoard")
    private String getBoard(@RequestParam(defaultValue = "") String id, BoardVO paramVO, Model model) throws IOException, ServletException {

        if(id.isEmpty()) {
            return "board/getBoardList";
        }

        paramVO.setId(Long.parseLong(id));
        paramVO = boardService.getBoard(paramVO);
        paramVO.setCnt(paramVO.getCnt()+1);

        model.addAttribute("boardVO", paramVO);

        return "board/getBoard";
    }

    @RequestMapping("/deleteBoard_proc")
    private String deleteBoard_proc(@RequestParam(defaultValue = "") String id) throws IOException {

        if(!id.isEmpty()) {
            BoardVO boardVO = new BoardVO();
            boardVO.setId(Long.parseLong(id));

            boardService.deleteBoard(boardVO);
        }

        return "redirect:/board/getBoardList";
    }

    @RequestMapping("/addBoard_proc")
    private String addBoard_proc(BoardVO paramVO) throws IOException {

        paramVO.setRegDate(new Date());
        boardService.addBoard(paramVO);

        return "redirect:/board/getBoardList";
    }

    @RequestMapping("/addBoard")
    private String addBoard() throws ServletException, IOException {
        return "board/addBoard";
    }

    @RequestMapping("/getBoardList")
    private String getBoardList (BoardVO paramVO, Model model) throws ServletException, IOException {

        List<BoardVO> boardList = boardService.getBoardList(paramVO);

        model.addAttribute("boardList", boardList);

        return "board/getBoardList";
    }
}
