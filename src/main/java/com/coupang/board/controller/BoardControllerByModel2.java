//package com.coupang.board.controller;
//
//import com.coupang.board.service.BoardService;
//import com.coupang.board.vo.BoardVO;
//import org.springframework.stereotype.Controller;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Date;
//import java.util.List;
//
///**
// * Created by Coupang on 2014. 7. 7..
// */
//@Controller
//public class BoardControllerByModel2 extends HttpServlet{
//
//    BoardService boardService = new BoardService();
//
//    @Override
//    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//
//        String uri = req.getRequestURI();
//
//        if("/board/getBoardList".equals(uri)) {
//            getBoardList(req, res);
//        }
//        else if("/board/addBoard".equals(uri)) {
//            addBoard(req, res);
//        }
//        else if("/board/addBoard_proc".equals(uri)) {
//            addBoard_proc(req, res);
//        }
//        else if("/board/deleteBoard_proc".equals(uri)) {
//            deleteBoard_proc(req, res);
//        }else if("/board/getBoard".equals(uri)) {
//            getBoard(req, res);
//        } else if("/board/updateBoard_proc".equals(uri)) {
//            updateBoard_proc(req, res);
//        }
//    }
//
//    private void updateBoard_proc(HttpServletRequest req, HttpServletResponse res) throws IOException {
//        String id = req.getParameter("id");
//        String title = req.getParameter("title");
//        String content = req.getParameter("content");
//
//        BoardService boardService = new BoardService();
//
//        BoardVO boardVO = new BoardVO();
//        boardVO.setId(Long.parseLong(id));
//
//        boardVO = boardService.getBoard(boardVO);
//
//        boardVO.setTitle(title);
//        boardVO.setContent(content);
//
//        boardService.updateBoard(boardVO);
//        res.sendRedirect("/board/getBoardList");
//    }
//
//    private void getBoard(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
//        String id = req.getParameter("id");
//
//        BoardVO boardVO = new BoardVO();
//        boardVO.setId(Long.parseLong(id));
//        boardVO = boardService.getBoard(boardVO);
//        boardVO.setCnt(boardVO.getCnt()+1);
//
//        req.setAttribute("boardVO", boardVO);
//        req.getRequestDispatcher("/getBoard.jsp").forward(req, res);
//    }
//
//    private void deleteBoard_proc(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String id = request.getParameter("seq");
//
//        BoardVO boardVO = new BoardVO();
//        boardVO.setId(Long.parseLong(id));
//
//        BoardService boardService = new BoardService();
//        boardService.deleteBoard(boardVO);
//
//        response.sendRedirect("/board/getBoardList");
//    }
//
//    private void addBoard_proc(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String title = request.getParameter("title");
//        String content = request.getParameter("content");
//        String writer = request.getParameter("writer");
//
//        BoardVO boardVO = new BoardVO();
//        boardVO.setTitle(title);
//        boardVO.setWriter(writer);
//        boardVO.setContent(content);
//        boardVO.setRegDate(new Date());
//
//        boardService.addBoard(boardVO);
//
//        response.sendRedirect("/board/getBoardList");
//        return;
//    }
//
//    private void addBoard(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        req.getRequestDispatcher("/addBoard.jsp").forward(req, res);
//    }
//
//    private void getBoardList (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
//        String searchCondition = req.getParameter("searchCondition");
//        String searchKeyword = req.getParameter("searchKeyword");
//
//        BoardVO vo = new BoardVO();
//        vo.setSearchCondition(searchCondition);
//        vo.setSearchKeyword(searchKeyword);
//
//        List<BoardVO> boardList = boardService.getBoardList(vo);
//
//        req.setAttribute("boardList", boardList);
//        req.getRequestDispatcher("/getBoardList.jsp").forward(req, res);
//    }
//}
