package com.coupang.board.vo;

import com.coupang.database.IValueObject;

import java.util.Date;

//서비스(sort 등 비즈니스 로직), DAO(액세스만 담당)을 구분

public class BoardVO implements IValueObject{

	private Long id;
	private String title;
	private String writer;
	private String content;
	private Date regDate;
	private int cnt;

	// 검색용 필드
	private String searchCondition;
	private String searchKeyword;



	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	public String getSearchCondition() {
		return searchCondition;
	}

	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	@Override
	public String toString() {
		return "BoardVO [id=" + id //
				+ ", title=" + title //
				+ ", writer=" + writer //
				+ ", content=" + content //
				+ ", regDate=" + regDate //
				+ ", cnt=" + cnt //
				+ "]";
	}
}
