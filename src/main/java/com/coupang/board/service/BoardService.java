package com.coupang.board.service;

import com.coupang.board.dao.BoardDao;
import com.coupang.board.vo.BoardVO;
import com.coupang.database.ICondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Coupang
 * Date: 2014. 7. 4.
 * Time: 오전 11:44
 * To change this template use File | Settings | File Templates.
 */
@Service
public class BoardService {

    @Autowired
    BoardDao boardDAO;

    public boolean addBoard(BoardVO vo) {
        return boardDAO.addBoard(vo);
    }

    public boolean updateBoard(BoardVO vo) {

        return boardDAO.updateBoard(vo);
    }

    public boolean deleteBoard(BoardVO vo) {
        return boardDAO.deleteBoard(vo);
    }

    public BoardVO getBoard(BoardVO vo) {
        return boardDAO.getBoard(vo);
    }

    public List<BoardVO> getBoardList(BoardVO vo) {

        final String searchCondition = vo.getSearchCondition();
        final String searchKeyword = vo.getSearchKeyword();

        if(vo.getSearchCondition() == null || vo.getSearchKeyword() == null) {
            return boardDAO.getBoardList(vo);
        }

        ICondition<BoardVO> iCondition = null;

        //나의 실수 : searchKeyword 자리에 searchCondition을 넣어놨다.
        //나의 실수 : equals 쓸 때 순서가 틀렸다.
        //나의 실수 : values를 다 받아서 add했다.

        if(vo.getSearchCondition() != null) {
            if("TITLE".equals(searchCondition)) {
                iCondition= new ICondition<BoardVO>() {
                    @Override
                    public boolean filter(BoardVO vo) {
                        return vo.getTitle().contains(searchKeyword);
                    }
                };
            } else {
                iCondition= new ICondition<BoardVO>() {
                    @Override
                    public boolean filter(BoardVO vo) {
                        return vo.getContent().contains(searchKeyword);
                    }
                };
            }
        }
        return boardDAO.getBoardList(iCondition);
    }

    public List<BoardVO> getBoardList(ICondition<BoardVO> iCondition) {
        return boardDAO.getBoardList(iCondition);
    }
}
