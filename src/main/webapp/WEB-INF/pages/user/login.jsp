<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
    window.onload = function(){
        var button = document.getElementById("add-btn");
        button.onclick = function() {
            location.href = "http://localhost:8080/user/addUser";
        }
    };
</script>
<title>로그인</title>
</head>
<body>
<center>
<h1>로그인</h1>
<hr>
<form action="/user/login_proc" method="post">
	<table border="1" cellpadding="0" cellspacing="0">
	<tr><td>아이디</td>
	    <td><input type="text" name="userId" value="${savedId}"/></td>
	</tr>
	<tr>
		<td>비밀번호</td>
		<td><input type="password" name="password"/></td>
	</tr>
        <%--Y냐 null이냐 --%>
        <%--체크박스는 체크를 안하면 서버로 넘어가질 않는다--%>
	<tr>
		<td>아이디 저장</td>
		<td><input type="checkbox" name="isSave" value="Y" ${isSave}/></td>
	</tr>
	<tr><td colspan="2" align="center"><input type="submit" value="로그인"/></td></tr>
    <tr><td colspan="2" align="center"><input type="button" value="회원가입" id="add-btn"/></td></tr>
    </table>
</form>
<hr>
</center>
</body>
</html>


<%--<%--%>
<%--String savedId = (String)request.getAttribute("savedId");--%>
<%--String isSave = (String)request.getAttribute("isSave");--%>
<%--%>--%>

<%--로그인--%>
<%--세션: 은행의 계좌같은 것! 어느 은행에서도 계좌번호를 통해 내 계좌에 연결할 수 있다! 특정 페이지에 접속하면 서버(와스)에 세션이라는 메모리 공간이 나를 위해 딱 하나 생김! 다른 페이지로 이동해도 세션은 유지! --%>
<%--쿠키: 내가 모르는 사이에 저장되어있다! 유효기간을 정할 수 있다! 리퀘스트가 일어날 때 항상 쿠키 정보를 가지고 올라간다! ex) id 저장하기, 오늘 하루동안 안보기--%>
<%--최초 접근 -> jsp 세션 여부 확인 -> 없으면 만듬 -> 세션의 유니크 아이디 발급해서 리스폰스로 내려줌 -> 쿠키에 세션값이 저장됨--%>
<%--SSO - Single Sign On -> 한번 로그인하면 뚱땅--%>
