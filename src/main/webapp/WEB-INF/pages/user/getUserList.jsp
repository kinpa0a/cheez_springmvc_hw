<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원 목록</title>
</head>
<body>
<center>
<h1>회원 목록</h1>
<!--  logout 자리  -->

<!-- 검색 시작 -->
<form action="/user/getUserList" method="post">
	<table border="1" cellpadding="0" cellspacing="0" width="700">
	<tr>
		<td align="right">
			<select name="searchCondition">
				<option value="USERID">아이디
				<option value="NAME">이름
				<option value="ROLE">역할
			</select>
			<input name="searchKeyword" type="text"/>
			<input type="submit" value="검색"/>
		</td>
	</tr>
	</table>
</form>
<!-- 검색 종료 -->

<table border="1" cellpadding="0" cellspacing="0" width="700">
<tr>
	<th bgcolor="orange" width="100">번호</th>
	<th bgcolor="orange" width="200">아이디</th>
	<th bgcolor="orange" width="150">이름</th>
	<th bgcolor="orange" width="150">롤</th>
</tr>


<!-- 목록 데이터 시작 -->
    <c:forEach var="userVO" items="${userList}">
        <tr>
            <td>${userVO.id}</td>
            <td align="left"><a href="/user/getUser?id=${userVO.id}">${userVO.userId}</a></td>
            <td><c:out value="${userVO.name}"></c:out></td>
            <td>${userVO.role}</td>
        </tr>
    </c:forEach>
<!-- 목록 데이터 끝 -->


</table>
<br>
<a href="/user/addUser">회원 등록</a>
</center>
</body>
</html>

<%--<%@ page import="com.coupang.user.service.UserService" %>--%>
<%--<%@ page import="com.coupang.user.vo.UserVO" %>--%>
<%--<%@ page import="java.util.List" %>--%>
<%--<%--%>

<%--List<UserVO> userList = (List<UserVO>) request.getAttribute("userList");--%>
<%--//    String searchCondition = request.getParameter("searchCondition");--%>
<%--//    String searchKeyword = request.getParameter("searchKeyword");--%>
<%--//--%>
<%--//    UserVO vo = new UserVO();--%>
<%--//    vo.setSearchCondition(searchCondition);--%>
<%--//    vo.setSearchKeyword(searchKeyword);--%>
<%--//--%>
<%--//    UserService userService = new UserService();--%>
<%--//    List<UserVO> UserList = userService.getUserList(vo);--%>
<%--%>--%>

<%--<%--%>
<%--for (UserVO userVO : userList) {--%>
<%--%>--%>
<%--<tr>--%>
<%--<td><%= userVO.getId() %></td>--%>
<%--<td align="left"><a href="/user/get_one?id=<%=userVO.getId()%>"><%= userVO.getUserId() %></a></td>--%>
<%--<td><%= userVO.getName() %></td>--%>
<%--<td><%= userVO.getRole() %></td>--%>
<%--</tr>--%>
<%--<%--%>
<%--}--%>
<%--%>--%>














