<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 목록</title>
</head>
<body>
<center>
<h1>글 목록</h1>
<!--  logout 자리  -->
<%@include file="../../../includePage.jsp"%>
<!-- 검색 시작 -->
<form action="/board/getBoardList" method="post">
	<table border="1" cellpadding="0" cellspacing="0" width="700">
	<tr>
		<td align="right">
			<select name="searchCondition">
				<option value="TITLE">제목
				<option value="CONTENT">내용
			</select>
			<input name="searchKeyword" type="text"/>
			<input type="submit" value="검색"/>
		</td>
	</tr>
	</table>
</form>
<!-- 검색 종료 -->

<table border="1" cellpadding="0" cellspacing="0" width="700">
<tr>
	<th bgcolor="orange" width="100">번호</th>
	<th bgcolor="orange" width="200">제목</th>
	<th bgcolor="orange" width="150">작성자</th>
	<th bgcolor="orange" width="150">등록일</th>
	<th bgcolor="orange" width="100">조회수</th>
</tr>

<!-- 목록 데이터 시작 -->

    <c:forEach var="boardVO" items="${boardList}">
    <tr>
        <td>${boardVO.id}</td>
        <td align="left"><a href="/board/getBoard?id=${boardVO.id}">${boardVO.title}</a></td>
        <td><c:out value="${boardVO.writer}"></c:out></td>
        <td><fmt:formatDate value="${boardVO.regDate}" pattern="yyyy/MM/dd"></fmt:formatDate></td>
        <td>${boardVO.cnt}</td>
    </tr>
</c:forEach>

<!-- 목록 데이터 끝 -->

</table>
<br>
<a href="/board/addBoard">새글 등록</a>
</center>
</body>
</html>


<%--<%@ page import="java.text.DateFormat" %>--%>
<%--<%@ page import="java.text.SimpleDateFormat" %>--%>
<%--<%@ page import="java.util.Date" %>--%>
<%--<%@ page contentType="text/html; charset=UTF-8"%>--%>
<%--<%!--%>
<%--선언부: 스크립틀릿에 !가 추가됨--%>
<%--변수--%>
<%--메서드--%>
<%--public String maskDate(Date date, String pattern) {--%>
<%--DateFormat df = new SimpleDateFormat(pattern);--%>
<%--return df.format(date);--%>
<%--}--%>
<%--%>--%>
<%--<%--%>
<%--List<BoardVO> boardList = (List<BoardVO>) request.getAttribute("boardList");--%>
<%--//    String searchCondition = request.getParameter("searchCondition");--%>
<%--//    String searchKeyword = request.getParameter("searchKeyword");--%>
<%--//--%>
<%--//    BoardVO vo = new BoardVO();--%>
<%--//    vo.setSearchCondition(searchCondition);--%>
<%--//    vo.setSearchKeyword(searchKeyword);--%>
<%--//--%>
<%--//    BoardService boardService = new BoardService();--%>
<%--//    List<BoardVO> boardList = boardService.getBoardList(vo);--%>
<%--%>--%>

<%--<%--%>
<%--//스크립틀릿 블록--%>
<%--//서블릿과 반대로 HTML 코드를 쓰다가 자바 코드를 쓰고 싶을 때 스크립틀릿을 쓴다!--%>

<%--for (BoardVO boardVO : boardList) {--%>
<%--%>--%>

<%--
${boardList} : EL
<c: : JSTL
EL은 page>request>session>applicaion을 돌면서 이름을 찾는다
nullpointexception이 뜨지 않는다. 없으면 그냥 안나온다
--%>
<%--<%--%>
<%--}--%>
<%--%>--%>

















