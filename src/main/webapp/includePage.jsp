<%@ page import="com.coupang.user.vo.UserVO" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<c:if test="${not empty USER}">
    <h3>
    ${USER.name}님 환영합니다!
    <a href="/user/logout">Logout</a>
    </h3>

</c:if>