<%--<%@ page import="com.coupang.board.service.BoardService" %>--%>
<%--<%@ page import="com.coupang.board.vo.BoardVO" %>--%>
<%--<%@ page import="java.util.Date" %>--%>
<%--&lt;%&ndash;--%>
  <%--Created by IntelliJ IDEA.--%>
  <%--User: Coupang--%>
  <%--Date: 2014. 7. 4.--%>
  <%--Time: 오후 3:13--%>
  <%--To change this template use File | Settings | File Templates.--%>
<%--&ndash;%&gt;--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%--<%--%>
    <%--//먼저 UTF-8로 인코딩 설정을 한 후에 데이터를 꺼내시오!--%>
    <%--//POST 방식만 처리가 가능하다.--%>
    <%--//GET 방식으로 한글을 넘길 때 -> conf/sever.xml에서 인코딩 설정--%>
<%--//    request.setCharacterEncoding("UTF-8");--%>

    <%--String title = request.getParameter("title");--%>
    <%--String content = request.getParameter("content");--%>
    <%--String writer = request.getParameter("writer");--%>

    <%--BoardVO boardVO = new BoardVO();--%>
    <%--boardVO.setTitle(title);--%>
    <%--boardVO.setWriter(writer);--%>
    <%--boardVO.setContent(content);--%>
    <%--boardVO.setRegDate(new Date());--%>

    <%--BoardService boardService = new BoardService();--%>
    <%--boardService.addBoard(boardVO);--%>

    <%--//forward : WAS 내에서 페이지를 이동해서 실행해라. 서버 내에서만 돌기 때문에 브라우저는 알 수 없다.--%>
    <%--//URL은 변경되지 않고 내용만 바뀜--%>
    <%--//등록, 수정, 삭제 때는 forward를 쓸 수 없다.--%>
    <%--//request.getRequestDispatcher("/getBoardList.jsp").forward(request, response);--%>

    <%--//서버가 브라우저에게 다시 해당 url을 요청해줘! 하고 부탁하는 것이다.--%>
    <%--//sendRedirect를 만나면 return을 해줘야 한당--%>
    <%--//서비스 밑에 메서드에서 실행되는 것이기 때문에 return을 한다.--%>
    <%--response.sendRedirect("/getBoardList.jsp");--%>
    <%--return;--%>
<%--%>--%>