<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>회원등록</title>
</head>
<body>
<center>
	<h3>회원 등록하기............</h3>
<!--  logout 자리  -->
	<hr>
		<form action="/user/add_proc" method="post">
		<table border="1" cellpadding="0" cellspacing="0">
			<tr>
				<td>아이디</td><td align="left"><input type="text" name="userId" size="20"/></td>
			</tr>
			<tr>
				<td>비밀번호</td><td align="left"><input type="password" name="password" size="20"/></td>
			</tr>
			<tr>
				<td>이름</td><td align="left"><input type="text" name="name" size="20"/></td>
			</tr>
			<tr>
				<td>역할</td>
				<td align="left">
					<select name="role">
						<option value="">== 선택 ==</option>
						<option value="Admin">관리자</option>
						<option value="User">회원</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" value=" 회원 등록 "/></td>
			</tr>
		</table>
		</form>
	<hr>
</center>
</body>
</html>