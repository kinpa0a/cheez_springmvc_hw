<%@page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 상세</title>
</head>
<body>
<center>
<h3>글 상세</h3>
<!--  logout 자리  -->
<hr>

<form action="/board/updateBoard_proc?id=${boardVO.id}" method="post">
<table border="1" cellpadding="0" cellspacing="0">
<tr>
<td>제목</td>
<td align="left"><input name="title" type="text" value="${boardVO.title}"/></td>
</tr>
<tr>
<td>작성자</td>
<td align="left">${boardVO.writer}</td>
</tr>
<tr>
<td>내용</td>
<td align="left"><textarea name="content" cols="40" rows="10">${boardVO.content}</textarea></td>
</tr>
<tr>
<td>등록일</td>
<td align="left">${boardVO.regDate}</td>
</tr>
<tr>
<td>조회수</td>
<td align="left">${boardVO.cnt}</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="글 수정"/>
</td>
</tr>
</table>
</form>
<hr>
<a href="/board/addBoard">글등록</a>&nbsp;&nbsp;&nbsp;
<a href="/board/deleteBoard_proc?id=${boardVO.id}">글삭제</a>&nbsp;&nbsp;&nbsp;
<a href="/board/getBoardList">글목록</a>
</center>
</body>
</html>

<%--<%@ page import="com.coupang.board.service.BoardService" %>--%>
<%--<%@ page import="com.coupang.board.vo.BoardVO" %>--%>
<%--<%--%>
<%--BoardVO boardVO = (BoardVO) request.getAttribute("boardVO");--%>
<%--String id = request.getParameter("id");--%>

<%--BoardService boardService = new BoardService();--%>
<%--BoardVO boardVO = new BoardVO();--%>
<%--boardVO.setId(Long.parseLong(id));--%>
<%--boardVO = boardService.getBoard(boardVO);--%>
<%--boardVO.setCnt(boardVO.getCnt()+1);--%>
<%--%>--%>






